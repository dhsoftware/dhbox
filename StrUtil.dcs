!-----------------------------------------------------------------------------------------------------------------------------!
! This software is distributed free of charge and WITHOUT WARRANTY. You may distribute it to others provided that you         !
! distribute the complete unaltered installation file provided by me at the dhsoftware.com.au web site, and that you do so    !
! free of charge (This includes not charging for distribution media and not charging for any accompanying software that is on !
! the same media or contained in the same download or distribution file).                                                     !
! If you wish to make any charge at all you need to obtain specific permission from me.                                       !
!                                                                                                                             ! 
! Whilst it is free (or because of this) I would like and expect that if you can think of any improvements or spot any bugs   !
! (or even spelling or formatting errors in the documentation) that you would let me know.  Your feedback will help with      !
! future development of the macro.                                                                                            !
!                                                                                                                             ! 
! Whilst the source code of the macro may be available for download, it is not 'open source'. You must not make changes and   !
! then make a competing product available to others. You can make changes for your own (or your company's) use, however in    !
! general I would prefer that you let me know of your requirements so that I can consider including them in a future release  !
! of my software. If in doubt please contact me.                                                                              !
!                                                                                                                             ! 
! David Henderson   dhsoftware1@gmail.com                                                                                     ! 
!-----------------------------------------------------------------------------------------------------------------------------!

MODULE StrUtil;

# INCLUDE '..\\..\\Inc\\_misc.inc'

# INCLUDE 'ErrUtil.inc'
# INCLUDE 'Lang.inc'



PROCEDURE msg_OK (msg : str255); BUILTIN 245;
FUNCTION msg_dlg (msg : str255; msgDlgType, msgDlgButtons : integer) : integer; BUILTIN 246;

FUNCTION BlankString (s : string): boolean;		PUBLIC;
VAR
	i	: integer;
	s1: string(1);
BEGIN
		if strlen(s) = 0 then
			return true;
		end;
		for i := 1 to strlen(s) do
			strsub (s, i, 1, s1);
			if not strcomp (s1, ' ', 1) then
				return false;
			end;
		end;
		return true;

END BlankString;

FUNCTION Numeric (s : string; allowdecimal : boolean) : boolean; PUBLIC;
VAR
	i : integer;
	s8 : str8;
BEGIN
	for i := 1 to strlen (s) do
		strsub (s, i, 1, s8);
		if strcomp (s8, '.', 1) then
			if not allowdecimal then
				return false;
			end;
		else
			if strcomp (s8, '0', 1) or
				 strcomp (s8, '1', 1) or
				 strcomp (s8, '2', 1) or
				 strcomp (s8, '3', 1) or
				 strcomp (s8, '4', 1) or
				 strcomp (s8, '5', 1) or
				 strcomp (s8, '6', 1) or
				 strcomp (s8, '7', 1) or
				 strcomp (s8, '8', 1) or
				 strcomp (s8, '9', 1) then
				! nothing
			else
				return false;
			end;
		end;
	end;
	return true;
END Numeric;


PROCEDURE SafeStrCat (str1 : IN OUT string; str2 : string);	PUBLIC;
! (only safe if str1 is 255 long)
VAR
	tempstr : str255;
	logfile : file;
	res	: integer;
BEGIN
	IF strlen(str1) + strlen(str2) <= 255 THEN
		strcat (str1, str2);
		return;
	END;

	getpath (tempstr, pathtmp);
	strcat (tempstr, 'dhStrings.log');
	res := f_open (logfile, tempstr, true, fmode_write);
	res := f_wrstr (logfile, 'STRCAT TOO LONG');
	res := f_wrln (logfile);
	res := f_wrstr (logfile, ' Str1: ');
	res := f_wrstr (logfile, str1);
	res := f_wrln (logfile);
	res := f_wrstr (logfile, ' Str2: ');
	res := f_wrstr (logfile, str2);
	res := f_close (logfile);

	strassign (tempstr, str2);
	while strlen(str1) + strlen(tempstr) > 255 do
		strdel (tempstr, strlen(tempstr), 1);
	end;
	strcat (str1,tempstr);
END SafeStrCat;

PROCEDURE SafeStrIns (dest : IN OUT string; source : string; pos : integer);	PUBLIC;
VAR
	tempstr : str255;
	logfile : file;
	res : integer;
BEGIN
	IF strlen(dest) + strlen(source) <= (sizeof (dest) - 1) THEN
		if pos > strlen (dest) then
			strcat (dest, source);
		else
			strins (dest, source, pos);
		end;
		return;
	END;

	getpath (tempstr, pathtmp);
	strcat (tempstr, 'dhStrings.log');
	res := f_open (logfile, tempstr, true, fmode_write);
	res := f_wrstr (logfile, 'STRINS TOO LONG');
	res := f_wrln (logfile);
	res := f_wrstr (logfile, ' Dest: ');
	res := f_wrstr (logfile, dest);
	res := f_wrln (logfile);
	res := f_wrstr (logfile, ' Source: ');
	res := f_wrstr (logfile, source);
	res := f_close (logfile);

	IF strlen (dest) = (sizeof (dest) - 1) THEN
		return;
	END;

	strassign (tempstr, source);
	while strlen(dest) + strlen(tempstr) > (sizeof (dest) - 1) do
		strdel (tempstr, strlen(tempstr), 1);
	end;
	strins (dest,tempstr, pos);
END SafeStrIns;

FUNCTION StartsWith (target : IN string; prefix : IN string; casesensitive : boolean) : boolean;	PUBLIC;
VAR
	i : integer;
	ct, cp	: string(1);
	t, p	: str255;

BEGIN
	if strlen(prefix) > strlen(target) then
		return false;
	end;
	strassign (t, target);
	strassign (p, prefix);
	if not casesensitive then
		strupcase (t, true);
		strupcase (p, true);
	end;
	for i := 1 to strlen(p) do
		strsub (t, i, 1, ct);
		strsub (p, i, 1, cp);
		if not strcomp (ct, cp, 1) then
			return false;
		end;
	end;
	return true;
END StartsWith;

FUNCTION EndsWith (target : string; suffix : string; casesensitive : boolean) : boolean;	PUBLIC;
VAR
	i : integer;
	ct, cs	: string(1);
	t, s		: str255;
	lendif	: integer;
	tempstr : str255;
BEGIN
	if strlen(suffix) > strlen(target) then
		return false;
	end;
	strassign (t, target);
	strassign (s, suffix);
	if not casesensitive then
		strupcase (t, true);
		strupcase (s, true);
	end;
	lendif := strlen(target) - strlen(suffix);
	for i := 1 to strlen(suffix) do
		strsub (t, lendif+i, 1, ct);
		strsub (s, i, 1, cs);
		if not strcomp (ct, cs, 1) then
			return false;
		end;
	end;
	return true;
END EndsWith;

FUNCTION ValidFileName (s : string; strname : string) : integer; 		PUBLIC;
!!! returns 0 if everything ok, otherwise returns 1 if user presses OK, -1 if user presses Cancel
VAR
	result	: boolean;
	msg			: str255;
	invalids: str255;
BEGIN
	result := true;
	invalids := '';
	msg := '';
	if strlen(s) = 0 then
		result := false;
		strassign (msg, strname);
		strcat (msg, ' may not be blank.\r');
	else
		if strpos (' ', s, 1) = 1 then
			result := false;
			strassign (msg, strname);
			strcat (msg, ' may not start with a space.\r\r');
		end;
		if strpos ('|', s, 1) > 0 then
			strcat (invalids, 'pipe (|), ');
		end;
		if strpos ('\\', s, 1) > 0 then
			strcat (invalids, 'backslash (\\), ');
		end;
		if strpos ('<', s, 1) > 0 then
			strcat (invalids, 'angle bracket (<), ');
		end;
		if strpos ('>', s, 1) > 0 then
			strcat (invalids, 'angle bracket (>), ');
		end;
		if strpos ('*', s, 1) > 0 then
			strcat (invalids, 'asterisk (*), ');
		end;
		if strpos ('?', s, 1) > 0 then
			strcat (invalids, 'question mark (?), ');
		end;
		if strpos ('/', s, 1) > 0 then
			strcat (invalids, 'slash (/), ');
		end;
		if strpos (':', s, 1) > 0 then
			strcat (invalids, 'colon (:), ');
		end;
		if strpos ('"', s, 1) > 0 then
			strcat (invalids, 'quote ("), ');
		end;
		if strlen(invalids) > 0 then
			result := false;
			strdel (invalids, strlen(invalids)-1, 2);
			strcat (msg, strname);
			strcat (msg, ' may not contain the following characters:\r');
			strcat (msg, invalids);
		end;
	end;

	if not result then
		if msg_dlg (msg, 2, 2) = 4 then
			return 1;
		else
			return -1;
		end;
	end;

	return 0;

END ValidFileName;


PROCEDURE AppendDateTimeStamp (Str : IN OUT String;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);	PUBLIC;
VAR
	y,m,d,h,mi,s,hu	: integer; !year, month, day, hours, minutes, seconds, hundredths
	tempstr				: str255;

	PROCEDURE Append2DigitStr (num : IN integer);
	!!! actually allows more than 2 digits, but inserts a zero at the front of the string if it is less than 2 digits
	BEGIN
		cvintst (num, tempstr);
		if strlen(tempstr)<2 then
			strins (tempstr, '0', 1);
		end;
		strcat (str, tempstr);
	END Append2DigitStr;

BEGIN
	if not (doDate or doTime) then
		return;
	end;
	ReadClock (y, m, d, h, mi, s, hu);
	if doDate then
		Append2DigitStr (y);
		Append2DigitStr (m);
		Append2DigitStr (d);
		if doTime and spacebetween then
			strcat (str, ' ');
		end;
	end;
	if DoTime then
		Append2DigitStr (h);
		if timecolons then
			strcat (str, ':');
		end;
		Append2DigitStr (mi);
		if timecolons then
			strcat (str, ':');
		end;
		Append2DigitStr (s);
		if timecolons and dohundredths then
			strcat (str, ':');
		end;
		if dohundredths then
			Append2DigitStr (hu);
		end;
	end;
End AppendDateTimeStamp;


PROCEDURE RemoveChars (str : IN OUT string; CharsToRemove : string);	PUBLIC;
VAR
	ndx, pos		: integer;
	pat				: string(1);
BEGIN
	FOR ndx := 1 to strlen(CharsToRemove) DO
		strsub(CharsToRemove, ndx, 1, pat);
		pos := strpos (pat, str, 1);
		WHILE pos > 0 DO
			strdel (str, pos, 1);
			pos := strpos (pat, str, 1);
		END;
	END;
END RemoveChars;

PROCEDURE GetClrName (clr : IN integer; clrname : OUT string);	PUBLIC;
! this replaces the standard clrGetName procedure. The clrGetName proc will output
! a maximum of 8 character string, which means that the result is sometimes misleading
! due to the string being truncated (e.g. Color_085 gets truncated to Color_08)
VAR
	str, str1	: str80;
BEGIN
	clrGetName (clr, str);
	if (clr > 99) and strpos ('Color_', str, 1) > 0 then
		cvintst (clr, str1);
		strins (str1, 'Color_', 1);
		if StartsWith (str1, str, false) then
			clrname := str1;
		else
			clrname := str;
		end;
	else
		clrname := str;
	end;

END GetClrName;


FUNCTION MyStrComp (str1, str2 : IN string; ignorecase : boolean) : integer;	PUBLIC;
!!! return -1 if str1 < str2, 1 if str1 > str2, 0 if they are equal)
VAR
		i, len1, len2, minlen : integer;
		s1, s2 : str255;
BEGIN
	strassign (s1, str1);
	strassign (s2, str2);
	if ignorecase then
		strupcase (s1, true);
		strupcase (s2, true);
	end;

	len1 := strlen (s1);
	len2 := strlen (s2);
	if len1 < len2 then
		minlen := len1;
	else
		minlen := len2;
	end;

	for i := 1 to minlen do
		if s1[i] < s2[i] then
			return -1;
		elsif s1[i] > s2[i] then
			return 1;
		end;
	end;

	if len1<len2 then
		return -1;
	elsif len1 > len2 then
		return 1;
	else
		return 0;
	end;
END MyStrComp;

END StrUtil.
