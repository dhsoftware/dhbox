# README #

Copyright (c) David Henderson 2019 (please see licence conditions at top of main modules)

### What is this repository for? ###

Classic DCAL macro to draw a box in DataCAD (www.datacad.com). Also works with STI Spirit (www.softtech.de)

### How do I get set up? ###

NSIS scripts for the install are included in the Install folder. In addition to copying the compiled macro to the DataCAD macro folder, it is necessary to create a dhsoftware folder within DataCAD's Support Files folder. The files from the checked in Support Files folder should be placed there together with the pdf instruction document (which can be created from the provided dhBox.docx file in the doc folder)
### Contribution guidelines ###

Please contact me if you would like to contribute to this project.

### Who do I talk to? ###

David Henderson (dhsoftware1@gmail.com)
web site: dhsoftware.com.au